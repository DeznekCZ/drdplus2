/**
 * The DnD5e game system for Foundry Virtual Tabletop
 * A system for playing the fifth edition of the worlds most popular roleplaying game.
 * Author: Atropos
 * Software License: GNU GPLv3
 * Content License: https://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf
 * Repository: https://gitlab.com/foundrynet/dnd5e
 * Issue Tracker: https://gitlab.com/foundrynet/dnd5e/issues
 */

import { DRDP2 } from "./module/config.js"
import DRDP2Item from "./module/item/entity.js"
import DRDP2ItemSheet from "./module/item/sheet.js"
import DRDP2CharacterActorSheet from "./module/actor/sheets/character.js"
import { preloadHandlebarsTemplates } from "./module/templates.js"
import DRDP2Actor from "./module/actor/entity.js";
import { registerSystemSettings } from "./module/settings.js";

console.log(DRDP2.ASCII);

Hooks.once("init", function () {
    console.log("DrD+2 | Initializing of Draci Doupe +2");

    CONFIG.DRDP2 = DRDP2;

    Items.unregisterSheet("core", ItemSheet)
    Items.registerSheet("drdp2", DRDP2ItemSheet, {
        makeDefault: true,
        label: "DRDP2.SheetClassItem"
    });

    Actors.unregisterSheet("core", ActorSheet)
    Actors.registerSheet("drdp2", DRDP2CharacterActorSheet, {
        types: ["character"],
        makeDefault: true,
        label: "DRDP2.SheetClassCharacter"
    });

    // Register System Settings
    registerSystemSettings();

    preloadHandlebarsTemplates();

    CONFIG.Actor.entityClass = DRDP2Actor;
    CONFIG.Item.entityClass = DRDP2Item;
    CONFIG.Combat.initiative.formula = "@attributes.combat.init.melee.value";
});
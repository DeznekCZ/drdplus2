// JavaScript source code

export default class Util {
    static add(source, target) {
        for (var key in source) {
            let val = source[key];
            let final = target;

            while (key.includes(".")) {
                let pre = key.split(".", 1)[0];
                key = key.substring(pre.length + 1);

                if (!(pre in final)) {
                    final = {};
                }

                final = final[pre];
            }

            let type = typeof val

            if (type === "number" || type === "string") {
                final[key] += val;
            } else if (type === "object") {
                Util.add(val, target[key]);
            }
        }
    }

    static isTrue(path, data) {
        return true == Util.getValue(path, data);
    }

    static getValue(path, data) {
        let keys = path.split(".");
        let object = data;

        for (var i = 0; i < keys.length; i++) {
            let key = keys[i];
            if (key in object) {
                object = object[key];
            } else
                if (key.includes("[")) {
                    let [objKey, indexKey] = key.split("[");
                    indexKey = Number.parseInt(
                        indexKey.substr(0, indexKey.length - 1)
                    );

                    object = (object[objKey] ?? [null])[indexKey];
                } else {
                    return null;
                }
        }

        return object;
    }

    /**
     * Compares value to both arguments and returs true if all matches image
     * @param {any} image requested value
     * @param {any} arg1 first argument
     * @param {any} arg2 second argument
     */
    static same(image, arg1, arg2) {
        return image === arg1 && image === arg2;
    }

    /**
     * Compares value to both arguments and returs true if at least one match image
     * @param {any} image requested value
     * @param {any} arg1 first argument
     * @param {any} arg2 second argument
     */
    static one(image, arg1, arg2) {
        return image === arg1 && image === arg2;
    }

    static tableLives(value) {
        return Util.baseTable(value - 10, { min: 1, round: true });
    }

    static tableDistanceKm(value) {
        return Util.baseTable(value - 20) / 1000;
    }

    static tableDistanceM(value) {
        return Util.baseTable(value - 20);
    }

    static tableWeight(value) {
        return Util.baseTable(value);
    }

    /**
     * Converts real distance to bonus
     * @param {any} value ammount in metres
     */
    static detableDistance(value) {
        return Util.baseDetable(-20, value);
    }

    /**
     * Converts real weight to bonus
     * @param {any} value ammount in kilograms
     */
    static detableWeight(value) {
        return Util.baseDetable(0, value);
    }

    /**
     * Base table for
     *   0- 9 10, 11, 12, 14, 16, 18, 20, 22, 25, 28,
     *  10-19 32, 36, 40, 45, 50, 56, 63, 70, 80, 90
     * @param {any} index
     */
    static baseTable(index, options = { min: NaN, round: false }) {
        let ni = index;
        let mult = 1;

        while (ni >= 20) {
            ni -= 20;
            mult *= 10;
        }

        while (ni < 0) {
            ni += 20;
            mult /= 10;
        }

        // Getting value
        let value = [
            /* 0- 9*/ 10, 11, 12, 14, 16, 18, 20, 22, 25, 28,
            /*10-19*/ 32, 36, 40, 45, 50, 56, 63, 70, 80, 90
        ][ni] * mult;

        // Options check
        if (options.round) {
            value = Math.round(value);
        }
        if (options.min != NaN && value < options.min) {
            value = options.min;
        }
        return value;
    }

    static baseDetable(base, value, options = { min: NaN, round: false }) {
        const data = [
            /* 0- 9*/ 10, 11, 12, 14, 16, 18, 20, 22, 25, 28,
            /*10-19*/ 32, 36, 40, 45, 50, 56, 63, 70, 80, 90
        ];
        let index = 0;
        let v = value;
        let imin = 19;
        let imax = 0;

        if (value <= 0) {
            return 0 - 40 - base;
        }

        while (v >= 100) {
            index += 20;
            v /= 10;
        }

        while (v > 0 && v < 10) {
            index -= 20;
            v *= 10;
        }

        let min = data[0] * 10;
        let max = data[19] / 10;

        // get bottom
        for (var i = 19; i >= 0; i--) {
            let di = data[i];
            if (di > v) {
                min = di;
                imin--;
            } else {
                break;
            }
        }

        // get top
        for (var i = 0; i < 20; i++) {
            let di = data[i];
            if (di < v) {
                max = di;
                imax++;
            } else {
                break;
            }
        }

        if (imin == imax) {
            return index + imin - base;
        }
        else if (imax > imin) {
            if ((imax - imin) == 1) {
                if ((max - v) > (v - min)) {
                    return index + imin - base;
                }
                else {
                    return index + imax - base;
                }
            }
            else if ((imax - imin) == 2) {
                return index + imin + 1 - base;
            }
            else {
                throw new Error("Unexpected indices");
            }
        }
        else {
            // case of options.round = true

        }
    }

    static getData(root, string, create = null) {
        let path = string.split(".", 2);
        let key = path[0];

        if (path.length == 1) {
            if (!(key in root) && !!create) {
                root[key] = create;
            }
            return root[key];
        } else {
            let rest = string.substring(key.length + 1);

            if (key in root) {
                return Util.getData(root[key], rest, create);
            } else if (!!create) {
                root[key] = {};
                return Util.getData(root[key], rest, create);
            } else {
                throw new Error(`Path in root is not specied: ${string}`)
            }
        }
    }

    static toUpdateObject(rootString, objectData) {
        let updates = {};

        this._toUpdateObject(updates, rootString, objectData)

        return updates;
    }

    static _toUpdateObject(updates, rootString, objectData) {
        if (typeof objectData === "array") {
            updates[rootString] = objectData;
        } else if (typeof objectData === "object") {
            updates[rootString] = {}
            for (var key in objectData) {
                this._toUpdateObject(updates, `${rootString}.${key}`, objectData[key]);
            }
        } else {
            updates[rootString] = objectData;
        }
    }

    static async skillRef(data, actor) {
        let key = (data.pack ?? "") + data.id;
        let hasNotSkill = true;

        let actorSkills = actor.itemTypes["skill-link"];
        for (var skillKey = 0; skillKey < actorSkills.length; skillKey++) {
            let skill = actorSkills[skillKey];
            let skillData = skill.data.data;
            let otherKey = (skillData.pack ?? "") + skillData.origin;
            if (otherKey === key) {
                let reference = (await Util.getItemFromRef(skill));
                let maxLevel = reference.data.data.level ?? 3;

                if (skillData.level == maxLevel) {
                    ui.notifications.error(game.i18n.format("DRDP2.SkillHasFullLevel", { skillName: reference.name }));
                } else {
                    skill.update({
                        "data.level": skillData.level + 1
                    });
                }
                hasNotSkill = false;
            }
        }

        if (hasNotSkill) {
            await actor.createOwnedItem({
                type: "skill-link",
                name: data.name,
                img: data.img,
                data: {
                    origin: data.id,
                    pack: data.pack ?? "",
                    level: 1
                }
            }, { renderSheet: false })
        }
    }

    static async skillRefCopy(data, actor) {
        let key = (data.data.pack ?? "") + data.data.origin;

        let actorSkills = actor.itemTypes["skill-link"];
        for (var skillKey = 0; skillKey < actorSkills.length; skillKey++) {
            let skill = actorSkills[skillKey];
            let skillData = skill.data.data;
            let otherKey = (skillData.pack ?? "") + skillData.origin;
            if (otherKey === key) {
                actor.deleteOwnedItem(skill._id);
                break;
            }
        }

        actor.createOwnedItem(data);
    }

    static async itemRef(data, actor) {
        let itemData = {
            type: "item-link",
            name: data.name,
            img: data.img,
            data: {
                origin: data.id,
                pack: data.pack ?? "",
                mods: {},
                contained: data.contained,
                owner: { _id: actor.id }
            }
        };
        let newItem = await actor.createOwnedItem(
            itemData,
            { renderSheet: false }
        );

        return newItem;
    }

    static async raceRef(data, actor) {
        let newItem = await actor.createOwnedItem({
            type: "race-link",
            name: data.name,
            img: data.img,
            data: {
                origin: data.id,
                pack: data.pack ?? ""
            }
        }, { renderSheet: false })

        return newItem;
        //return actor.items.get(newItem._id).update({
        //    "data.origin": data.id,
        //    "data.pack": data.pack ?? ""
        //});
    }

    static async backgroundRef(data, itemData, actor) {
        let newItem = await actor.createOwnedItem({
            type: "background-link",
            name: data.name,
            img: data.img,
            data: {
                origin: data.id,
                pack: data.pack ?? "",
                type: itemData.type,
                attrinutes: {}
            }
        }, { renderSheet: false })

        return newItem;
        //return actor.items.get(newItem._id).update({
        //    "data.origin": data.id,
        //    "data.pack": data.pack ?? ""
        //});
    }

    static async getItemFromRef(ref, action = null) {
        const origin = ref.data.data.origin;
        const pack = ref.data.data.pack ?? "";
        let item = null;

        if (!action) {
            action = function (i) { return i };
        }

        if (pack.length == 0) {
            item = await game.items.get(origin);
            if (!!item) {
                return await action(item);
            } else {
                console.error(`Item ${origin} not found!`);
                return null;
            }
        } else {
            let packData = game.packs.get(pack);
            if (!!packData) {
                item = await packData.getEntity(origin);

                if (!!item) {
                    return await action(item);
                } else {
                    console.error(`Item ${pack}.${origin} not found!`);
                    return null;
                }
            } else {
                console.error(`Pack ${pack} not found!`);
                return null;
            }
        }
    }

    static romans(origin) {
        const Rules = [
            [1000, "M"],
            [900, "CM"],
            [500, "D"],
            [400, "CD"],
            [100, "C"],
            [90, "XC"],
            [50, "L"],
            [40, "XL"],
            [10, "X"],
            [9, "IX"],
            [5, "V"],
            [4, "IV"],
            [1, "I"]
        ];

        let value = origin;
        let romans = "";
        let count = Rules.length;
        let negative = value < 0;

        if (negative)
            value = 0 - value;

        for (var i = 0; value > 0 && i < count; i++) {
            let number = Rules[i][0]
            while (value >= number) {
                value -= number;
                romans += Rules[i][1];
            }
        }

        return (negative ? "-" : "") + romans;
    }

    static arabic(origin) {
        const Rules = [
            [900, "CM"],
            [1000, "M"],
            [400, "CD"],
            [500, "D"],
            [90, "XC"],
            [100, "C"],
            [40, "XL"],
            [50, "L"],
            [9, "IX"],
            [10, "X"],
            [4, "IV"],
            [5, "V"],
            [1, "I"]
        ];

        let value = origin;
        let arabic = 0;
        let count = Rules.length;
        let negative = value.substring(0, 1) === '-'

        if (negative)
            value = value.substring(1);

        for (var i = 0; value.length > 0 && i < count; i++) {
            let romans = Rules[i][1]
            while (value.length > 0 &&
                value.substring(0, romans.length) === romans) {

                value = value.substring(romans.length);
                arabic += Rules[i][0];
            }
        }

        return negative ? (0 - arabic) : arabic;
    }

    static getType(token) {
        const tof = typeof token;
        if (tof === "object") {
            if (token === null) return "null";
            let cn = token.constructor.name;
            if (["String", "Number", "Boolean", "Array", "Set"].includes(cn)) return cn;
            else if (/^HTML/.test(cn)) return "HTMLElement";
            else return "Object";
        }
        return tof;
    }

    static SUPLEMENT = "##MERGE_SUPLEMENT";

    static fixArrays(original) {
        let type = Util.getType(original);

        if (type == "Array") {
            let newArray = [];

            for (var i = 0; i < original.length; i++) {
                newArray.push(Util.fixArrays(original[i]));
            }

            return newArray;
        }
        else
            if (type == "Object") {
                let value = {};

                for (var key in original) {
                    if (key.includes("[")) {
                        let [objKey, indexKey] = key.split("[");
                        indexKey = Number.parseInt(
                            indexKey.substr(0, indexKey.length - 1)
                        );

                        let keyValue = Util.fixArrays(original[key]);
                        let array = value[objKey] = value[objKey] ?? [];

                        while (indexKey > array.length) {
                            array.push(Util.SUPLEMENT);
                        }

                        value[objKey].push(keyValue);
                    } else {
                        value[key] = Util.fixArrays(original[key]);
                    }
                }
                return value;
            } else {
                return original;
            }
    }

    static merge(origin, other) {
        // Skip Check because of unchanged values
        if (other == Util.SUPLEMENT) {
            return false;
        }

        let type = Util.getType(other);
        if (origin == null) {
            switch (type) {
                case "Array":
                    origin = [];
                    break;
                case "Object":
                    origin = {};
                    break;
                default:
                    return true;
            }
        }

        if (type == "Array") {
            for (var i = 0; i < other.length; i++) {
                let item = origin[i];

                let simpleType = item == null
                    || Util.merge(item, other[i]);
                if (simpleType) {
                    origin[i] = other[i];
                }
            }
        }
        else
            if (type == "Object") {
                for (var key in other) {
                    let item = origin[key];

                    let simpleType = item == null ||
                        Util.merge(origin[key], other[key]);
                    if (simpleType) {
                        origin[key] = other[key];
                    }
                }
            } else {
                return true;
            }

        return false; // SimpleType
    }

    static includes(arrayIterator, value) {
        let next = arrayIterator.next();
        while (!next.done) {
            if (next.value == value)
                return true;
            else
                next = arrayIterator.next();
        }
    }

    static async getItem(id, pack) {
        if (pack?.length > 0 ?? false) {
            let packData = game.packs.get(pack);
            return await packData.getEntity(id);
        } else {
            return game.items.get(id);
        }
    }

    static getComponent(type, components) {
        if (components == null) return null;
        for (var i = 0; i < components.length; i++) {
            let component = components[i];
            if (component.type === type)
                return component;
        }
    }

    static eachComponent(type, components, action) {
        if (components == null) return null;

        for (var i = 0; i < components.length; i++) {
            let component = components[i];
            if (component.type === type)
                action(component);
        }
    }

    static async eachComponentAsync(type, components, action) {
        if (components == null) return null;

        for (var i = 0; i < components.length; i++) {
            let component = components[i];
            if (component.type === type)
                await action(component);
        }
    }
}
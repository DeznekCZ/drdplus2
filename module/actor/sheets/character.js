//import ActorSheet5e from "./base.js";
//import Actor5e from "../entity.js";
import Util from "../../copyutil.js"

/**
 * An Actor sheet for player character type actors.
 * Extends the base ActorSheet5e class.
 * @type {ActorSheet5e}
 */
export default class DRDP2CharacterActorSheet extends ActorSheet {

    /** @override */
    get template() {
        if (!game.user.isGM && this.actor.limited) return "systems/dnd5e/templates/actors/limited-sheet.html";
        return `systems/drdp2/templates/actors/${this.actor.data.type}-sheet.html`;
    }
    /**
     * Define default rendering options for the NPC sheet
     * @return {Object}
     */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["drdp2", "sheet", "actor", "character"],
            width: 720,
            height: 680,
            scrollY: [
                ".inventory .inventory-list",
                ".skills .inventory-list",
                ".spellbook .inventory-list",
                ".effects .inventory-list"
            ],
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "description" }]
        });
    }

    /* -------------------------------------------- */

    /**
     * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
     */
    async getData() {
        const data = super.getData();
        this.onCount = [];

        data.config = CONFIG.DRDP2;

        let xp = data.data.details.xp;
        xp.max = Util.tableLives(data.data.details.level + 15);
        xp.pct = Math.min(Math.max(100 * xp.value / xp.max, 0), 100);

        let d2 = function (arg1, arg2) {
            return Math.ceil(((arg1 ?? 0) + (arg2 ?? 0)) / 2);
        }
        let finalize = function (arg, bonus) {
            arg.value = (arg.base ?? 0) + (arg.bonus ?? 0) + (bonus ?? 0);
        }
        let reset = function (arg, keys = ["base", "bonus"]) {
            for (var i = 0; i < keys.length; i++) {
                arg[keys[i]] = 0;
            }
        }

        let attr = data.data.attributes;

        // Clear out
        reset(attr.body.sil);
        reset(attr.body.obr);
        reset(attr.body.zrc);
        attr.body.bonus = 0;

        reset(attr.mind.vol);
        reset(attr.mind.int);
        reset(attr.mind.chr);
        attr.mind.bonus = 0;

        reset(attr.reaction.odl);
        reset(attr.reaction.vdr);
        reset(attr.reaction.rch);
        reset(attr.reaction.sms);
        reset(attr.reaction.mag);

        reset(attr.senses["sms-zrk"]);
        reset(attr.senses["sms-slu"]);
        reset(attr.senses["sms-cch"]);
        reset(attr.senses["sms-hmt"]);
        reset(attr.senses["sms-cht"]);
        reset(attr.senses["sms-vnm"]);

        reset(attr.visage.krs);
        reset(attr.visage.neb);
        reset(attr.visage.dst);

        reset(attr.sizes.vel);
        reset(attr.sizes.hmp, ["value", "combat", "other"]);

        attr.skills.points = 0;
        attr.skills.used = 0;

        // Races - effects
        const raceLink = this.actor.itemTypes["race-link"].find(_ => true);
        const race = !!raceLink ? await Util.getItemFromRef(raceLink) : null;
        if (!!race) {
            Util.add(race.data.data.attributes, attr)
        }

        // Background
        await this._prepareBackground(attr, data);

        // Skills - effects
        await this._prepareSkills(attr, data);

        // Items - effects
        await this._prepareItems(attr, data);

        // Effects

        // Statistics
        let height = data.data.details.height;
        height.value = Util.detableDistance(height.display / 100);
        finalize(attr.sizes.vel, data.data.details.height.value);
        height.fix = Math.floor((height.value + 1) / 3) - 1;

        attr.sizes.vel.value =
            data.data.details.weight.value
            - 2 * data.data.details.height.value + 80;

        data.data.details.weight.value = Util.detableWeight(data.data.details.weight.display);

        if (Util.isTrue("data.details.combat-equipment", data)) {
            attr.sizes.hmp.real =
                attr.sizes.hmp.combat
                + data.data.details.weight.display;
        } else {
            attr.sizes.hmp.real =
                attr.sizes.hmp.combat
                + attr.sizes.hmp.other
                + data.data.details.weight.display;
        }
        attr.sizes.hmp.value = Util.detableWeight(attr.sizes.hmp.real);

        finalize(attr.body.sil);
        finalize(attr.body.obr);
        finalize(attr.body.zrc);
        finalize(attr.mind.vol);
        finalize(attr.mind.int);
        finalize(attr.mind.chr);

        attr.reaction.odl.value = attr.reaction.odl.base + attr.body.sil.base;
        attr.reaction.vdr.value = attr.reaction.vdr.base + d2(attr.body.sil.base, attr.mind.vol.base);
        finalize(attr.reaction.rch, d2(attr.body.sil.value, attr.body.obr.value) + height.fix);
        finalize(attr.reaction.sms, attr.body.zrc.value);
        finalize(attr.reaction.mag, attr.mind.vol.value);

        finalize(attr.senses["sms-zrk"], attr.reaction.sms.value);
        finalize(attr.senses["sms-slu"], attr.reaction.sms.value);
        finalize(attr.senses["sms-cch"], attr.reaction.sms.value);
        finalize(attr.senses["sms-hmt"], attr.reaction.sms.value);
        finalize(attr.senses["sms-cht"], attr.reaction.sms.value);
        finalize(attr.senses["sms-vnm"], attr.reaction.sms.value);

        finalize(attr.visage.krs, d2(attr.body.obr.value, attr.body.zrc.value) + d2(attr.mind.chr.value));
        finalize(attr.visage.neb, d2(attr.body.sil.value, attr.mind.vol.value) + d2(attr.mind.chr.value));
        finalize(attr.visage.dst, d2(attr.mind.int.value, attr.mind.vol.value) + d2(attr.mind.chr.value));

        attr.hp.lines = 3 + (attr.hp['bonus-lines'] ?? 0);
        attr.hp.line = Util.tableLives(6 + attr.reaction.odl.value);
        attr.hp.total = attr.hp.lines * attr.hp.line;

        // alternative 3 * Math.floor(attr.hp.damage / attr.hp.line)
        attr.hp.pain.base = (
            attr.hp.line < (attr.hp.damage ?? 0) ? 3 : 0
        );

        finalize(attr.hp.pain);


        // Finalize values depend on attributes
        for (var i = 0; i < this.onCount.length; i++) {
            this.onCount[i](attr, data);
        }

        return data;
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers
    /* -------------------------------------------- */

    /**
     * Activate event listeners using the prepared sheet HTML
     * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
     */
    activateListeners(html) {
        html.find('.race-info').click(this._onRaceView.bind(this));
        html.find('.background-info').click(this._onBackgroundView.bind(this));

        if (!this.options.editable) return;

        if (this.actor.owner) {
            // Attribute raw roll
            html.find('.ability-name').click(this._onRollAttribute.bind(this));

            // Lives editing
            html.find('.lives').click(this._onLivesEdit.bind(this));

            // Item handling
            html.find('.item-link').click(this._onItemLink.bind(this));
            html.find('.item-delete').click(this._onItemDelete.bind(this));

            // Skill level add or remove
            html.find('.item-learn').click(this._onSkillLearn.bind(this));
            html.find('.item-unlearn').click(this._onSkillUnlearn.bind(this));

            // Level handling
            html.find('.level-add').click(this._onLevelAdd.bind(this));
            html.find('.level-rem').click(this._onLevelRem.bind(this));

            // Item edit
            html.find('.item-edit').change(this._onItemEdit.bind(this));

            // Input focus and update
            const inputs = html.find("input");
            inputs.focus(ev => ev.currentTarget.select());
            inputs.addBack().find('[data-dtype="Number"]').change(this._onChangeInputDelta.bind(this));
        }

        // Otherwise remove rollable classes
        else {
            html.find(".rollable").each((i, el) => el.classList.remove("rollable"));
        }

        // Handle default listeners last so system listeners are triggered first
        super.activateListeners(html);
    }

    async _onRaceView(event) {
        event.preventDefault();
        const item = this.actor.getOwnedItem(event.currentTarget.dataset.id);

        Util.getItemFromRef(item, i => {
            i.sheet.render(true);
        });
    }

    async _onBackgroundView(event) {
        event.preventDefault();
        const item = this.actor.getOwnedItem(event.currentTarget.dataset.id);
        item.sheet.render(true);
    }

    async _onLivesEdit(event) {
        event.preventDefault();

        let dataset = event.currentTarget.dataset;
        let index = Number.parseInt(dataset.value);
        let totalDamage = index + (event.currentTarget.checked ? 1 : 0);
        let linethrs = this.actor.data.data.attributes.hp.line;

        const updates = {
            "data.attributes.hp.damage": totalDamage
        };

        return this._update(updates);
    }

    /**
     * Handle input changes to numeric form fields, allowing them to accept delta-typed inputs
     * @param event
     * @private
     */
    _onChangeInputDelta(event) {
        const input = event.target;
        const value = input.value;
        if (["+", "-"].includes(value[0])) {
            let delta = parseFloat(value);
            input.value = getProperty(this.actor.data, input.name) + delta;
        } else if (value[0] === "=") {
            input.value = value.slice(1);
        }
    }

    /**
   * Handle rolling an Ability check, either a test or a saving throw
   * @param {Event} event   The originating click event
   * @private
   */
    async _onRollAttribute(event) {
        event.preventDefault();
        let dataset = event.currentTarget.parentElement.dataset;
        let attribute = dataset.attribute;
        let set = dataset.set;

        let data = {}
        data.attribute = this.actor.data.data.attributes[set][attribute].value
        data[attribute] = data.attribute

        let roll = new Roll(`2d6 + @${attribute}`, data);
        try {
            roll.roll();
        } catch (err) {
            console.error(err);
            ui.notifications.error(`Dice roll evaluation failed: ${err.message}`);
            return;
        }

        if (roll.dice[0].total === 12) {
            let nextRoll = new Roll("d6", {})
            nextRoll.roll()
            while (nextRoll.dice[0].total > 3) {
                //roll.dice.add(nextRoll.dice[0]);
                nextRoll.reroll();
                //roll.result = roll.result + 1;
            }
        }
        else if (roll.total === 2) {
            let nextRoll = new Roll("d6", {})
            nextRoll.roll()
            while (nextRoll.dice[0].total < 4) {
                //roll.dice.add(nextRoll.dice[0]);
                nextRoll.reroll();
                //roll.result = roll.result - 1;
            }
        }


        let messageData = {
            speaker: this.actor,
            flavor: game.i18n.format("DRDP2.AttributeRoll", data)
        }
        roll.toMessage(
            messageData,
            game.settings.get("core", "rollMode"),
            true
        )
    }

    /* -------------------------------------------- */

    /** @override */
    async _onDropItem(event, data) {
        if (!this.actor.owner) return false;
        const item = await Item.fromDropData(data);
        const itemData = item.data;
        data.name = item.name;
        data.img = item.img;

        // Standard action - not owned and can be equiped
        // (!sameActor && (mainContainer || canContain))

        if (itemData.type === "race") {
            let raceLinks = this.actor.itemTypes["race-link"];
            while (raceLinks.length > 0) {
                let item = raceLinks.pop();
                await item.delete();
            }

            return Util.raceRef(data, this.actor);
        } else if (itemData.type === "race-link") {
            let raceLinks = this.actor.itemTypes["race-link"];
            while (raceLinks.length > 0) {
                let item = raceLinks.pop();
                await item.delete();
            }

            return this.actor.createOwnedItem(itemData);
        } else if (itemData.type === "background") {
            let backgroundLinks = this.actor.itemTypes["background"];
            while (backgroundLinks.length > 0) {
                let item = backgroundLinks.pop();
                await item.delete();
            }

            return this.actor.createOwnedItem(itemData);
        } else if (itemData.type === "skill") {
            return Util.skillRef(data, this.actor);
        } else if (itemData.type === "skill-link") {
            return Util.skillRefCopy(itemData, this.actor);
        } else if (itemData.type === "item" ||
            itemData.type === "item-link") {
            this._onDropItemTypeItem(event, data, item, itemData);
        }
    }

    async _onDropItemTypeItem(event, data, item, itemData) {
        item.reference = itemData.type === "item-link"
            ? await Util.getItemFromRef(item)
            : item;

        // Get closest container
        const containerHeader = event.target.closest(".inventory-header");
        const containerItems = event.target.closest(".item-list");
        const containerLists = event.target.closest(".inventory-list");
        data.contained = itemData.contained
            = (containerHeader ?? containerItems ?? containerLists)
                .dataset.container;

        const actor = this.actor;
        let sameActor = (data.actorId === actor._id) || (actor.isToken && (data.tokenId === actor.token.id));

        // Case: copy from different inventory
        let sameContainer = data.contained === (itemData.data.contained ?? "");
        let container = { link: null };
        let canContain = data.contained !== "" &&
            (sameContainer || await this._canContain(data.contained, item, container));
        let mainContainer = data.contained === "";
        let selfContaining = data.contained === itemData._id;

        if (selfContaining) {
            ui.notifications.error(
                game.i18n.localize("DRDP2.ContainerCycleInsert")
            );
            return;
        }
        // Owned but resorted
        if (sameActor && sameContainer) {
            return this._onSortItem(event, itemData);
        }
        // Owned bot moved
        if (sameActor && (canContain || mainContainer)) {
            this.actor.items.get(data.data._id).update({
                "data.contained": data.contained
            });
            return;
        }
        // Owned but is bad inventory
        if (sameActor && !mainContainer && !canContain) {
            if (container.link == null)
                container.link = this._getContainer(data.contained);

            ui.notifications.error(
                game.i18n.format("DRDP2.ContainerItemMissplaced", {
                    containerName: container.link.name,
                    itemName: item.name
                })
            );
            return;
        }
        // Not owned but is bad inventory
        if (!sameActor && !mainContainer && !canContain) {
            if (container.link == null)
                container.link = this._getContainer(data.contained);

            ui.notifications.error(
                game.i18n.format("DRDP2.ContainerItemMissplaced", {
                    containerName: container.link.name,
                    itemName: item.name
                })
            );
            return;
        }

        if (itemData.type === "item-link")
            return this.actor.createOwnedItem(itemData);
        if (itemData.type === "item")
            return Util.itemRef(data, this.actor);

    }

    async _getContainer(link) {
        return { link: await Util.getItemFromRef(containerLink) };
    }

    async _canContain(link, item, returnValue = { link: null }) {
        let containerLink = this.actor.items.get(link);
        let container = await Util.getItemFromRef(containerLink);
        returnValue.link = container;
        let containerData = container.data.data;

        let itemData = {
            origin: item.reference.data._id,
            pack: item.reference.data.pack ?? ""
        };
        let components = item.reference.data.data.components;

        let filters = (
            Util.getComponent("container", containerData.components)
            ?? { filters: [] }).filters;
        if (filters.length > 0) {
            let filter = filters[0];

            return await this._canFilterContain(filter, itemData, components);
        }

        return true;
    }

    async _canFilterContain(filter, itemData, components) {
        switch (filter.type) {
            case "items":
                let items = filter.items;
                for (var iid = 0; iid < items.length; iid++) {
                    let linkedItem = items[iid];

                    if (itemData.origin === linkedItem._id &&
                        (itemData.pack ?? "") === (linkedItem.pack ?? "")
                    ) {
                        return true;
                    }
                }
                return false;

            case "accept":
                for (var cid = 0; cid < components.length; cid++) {
                    let component = components[cid];
                    if (filter.types[component.type] ?? false)
                        return true;
                }
                return false;

            case "deny":
                for (var cid = 0; cid < components.length; cid++) {
                    let component = components[cid];
                    if (!(filter.types[component.type] ?? false))
                        return false;
                }
                return true; // if not denied

            case "and":
                let and = true;
                for (var i = 0; i < filter.filters; i++) {
                    and = and & this._canFilterContain(
                        filter.filters[i], itemData, components
                    );
                    if (!and) return false;
                }
                return true;

            case "or":
                let or = false;
                for (var i = 0; i < filter.filters; i++) {
                    or = or | this._canFilterContain(
                        filter.filters[i], itemData, components
                    );
                    if (or) return true;
                }
                return false;

            default:
                break;
        }
        return false;
    }

    async _onEffectAdded(effectItem) {
        _update()
    }

    async _update(updates) {
        return this.actor.update(updates);
    }

    async _prepareSkills(attributes, data) {
        const skillTemplates = {
            racial: { sort: 0, label: "DRDP2.SkillGroup.racial", items: [], dataset: { type: "skill-link" } },
            combat: { sort: 1, label: "DRDP2.SkillGroup.combat", items: [], dataset: { type: "skill-link" } },
            none: { sort: 2, label: "DRDP2.SkillGroup.none", items: [], dataset: { type: "skill-link" } },
            classgroup: { sort: 3, label: "DRDP2.SkillGroup.class", items: [], dataset: { type: "skill-link" } }
        };
        const skills = {};
        const knownSkills = {};

        let actorSkills = this.actor.itemTypes["skill-link"];
        for (var skillKey = 0; skillKey < actorSkills.length; skillKey++) {
            let skill = actorSkills[skillKey];
            if (!(skill.type)) continue;
            let skillData = skill.data.data;
            let reference = await Util.getItemFromRef(skill);
            let referenceData = reference.data.data;
            let group = referenceData.type ?? "none";
            let clazz = referenceData.class;
            knownSkills[(skillData.pack ?? "") + "." + skillData.origin]
                = skillData.level ?? 1;

            skill.data.name = reference.data.name;
            skill.data.img = reference.data.img;

            if (!(skill.free === true) && !(group.type === "racial")) {
                Util.add({ "skills.used": skillData.level ?? 1 }, attributes);
            }

            skill.reference = referenceData;
            let effects = referenceData.effects ?? {};
            for (var effectKey = 0; effectKey < effects.length; effectKey++) {
                let effect = effects[effectKey];
                if (effect.type === "attribute") {
                    Util.add(effect.attributes ?? {}, attributes)
                }
            }

            // Classes
            if (referenceData.type === "class") {
                if (!!referenceData.class) {
                    group = clazz.pack + "." + clazz._id;

                    if (!(group in skills)) {
                        let actorClassRef = await Util.getItemFromRef({
                            data: {
                                data: {
                                    origin: clazz._id,
                                    pack: clazz.pack
                                }
                            }
                        });

                        skills[group] = {
                            label: game.i18n.localize("DRDP2.SkillGroup.class") + ": " + actorClassRef.data.name,
                            sort: 4,
                            items: []
                        };
                    }
                } else {
                    group = "classgroup"
                }
            }

            // Standard groups
            if (!(group in skills)) {
                skills[group] = skillTemplates[group];
            }

            skills[group].items.push(skill);
        }

        data.knownSkills = knownSkills;
        data.skills = Object.values(skills);
        data.skills.sort((a, b) => {
            if (a.label > b.label) {
                return 1;
            }
            if (b.label > a.label) {
                return -1;
            }
            return 0;
        });
        data.skills.sort((a, b) => a.sort - b.sort);
    }

    async _prepareItems(attributes, data) {
        const itemTemplates = {
            others: { sort: 0, label: "DRDP2.ItemGroup.others", items: [], dataset: { type: "item-link" } },
        };
        const items = {};
        data.weight = {
            limit: 0, // later hook

            total: 0, // to count here
            combat: 0, // to count here
        }

        let actorItems = this.actor.itemTypes["item-link"];
        items["others"] = itemTemplates["others"];

        for (var itemKey = 0; itemKey < actorItems.length; itemKey++) {
            let item = actorItems[itemKey];
            let key = item.data._id;
            let count = item.data.data.quantity ?? 1;
            let reference = await Util.getItemFromRef(item);
            let components = reference.data.data.components;

            for (var compId = 0; compId < components.length; compId++) {
                let component = components[compId];
                if (component.type == "container") {
                    items[key] = {
                        key: key,
                        sort: 0,
                        label: item.name,
                        img: item.img,
                        items: [],
                        dataset: { type: "item-link" },
                        display: {
                            value: component.display.value,
                            count: component.display.count,
                            weight: component.display.weight
                        },
                        limit: {
                            count: component.countLimit * count,
                            weight: component.weightLimit * count
                        },
                        value: reference.data.data.price.value * count,
                        count: 0,
                        weight: 0
                    };
                    break;
                }
            }
        }

        for (var itemKey = 0; itemKey < actorItems.length; itemKey++) {
            let item = actorItems[itemKey];
            if (!(item.type)) continue;
            let itemData = item.data.data;
            let reference = await Util.getItemFromRef(item);
            let referenceData = reference.data.data;
            let group = referenceData.type ?? "others";
            let count = itemData.quantity ?? 1;

            item.reference = referenceData;
            item.weight = (itemData.quantity ?? 1) * (referenceData.weight ?? 0);
            item.price = (itemData.quantity ?? 1) * (referenceData.price.value ?? 0);
            let effects = referenceData.effects ?? {};
            for (var effectKey = 0; effectKey < effects.length; effectKey++) {
                let effect = effects[effectKey];
                if (effect.type === "attribute") {
                    Util.add(effect.attributes ?? {}, attributes)
                }
            }

            let weight = (referenceData.weight ?? 0) * count;

            if (itemData.combatEquiped) {
                data.weight.combat += weight;
            }
            data.weight.total += weight;

            // Standard groups
            if (!(group in items)) {
                items[group] = itemTemplates[group];
            }

            if (!!itemData.contained
                && itemData.contained != ""
                && !!items[itemData.contained]
            ) {
                let container = items[itemData.contained];
                if (container.display.weight)
                    container.weight += referenceData.weight
                        * (itemData.quantity ?? 1);
                if (container.display.value)
                    container.value += referenceData.price.value
                        * (itemData.quantity ?? 1);
                if (container.display.count)
                    container.count += itemData.quantity ?? 1;
                container.items.push(item);
            } else {
                itemData.contained = null;
                items[group].items.push(item);
            }
        }

        data.items = Object.values(items);

        // Create entry for attribute
        Util.add({ "body.sil.weightBonus": 0 }, attributes)
        this.onCount.push((a, d) => {
            let neededTotal = Util.detableWeight(d.weight.total);
            let neededCombat = Util.detableWeight(d.weight.combat);
            let sil = a.body.sil.value;
            let wbonus = Util.getValue("body.sil.weightBonus", a);
            if (isNaN(wbonus)) wbonus = 0;
            let isCombat = Util.getValue("details.combat-equipment", d);

            function getCounted(s, wc, z, b) {
                let wz = Util.tableWeight(s + z);
                let wb = Util.tableWeight(s + b);

                return Math.max(0, Math.min(100, ((wc - wb) / (wz - wb)) * 100));
            }

            let countedWeigth = isCombat ? d.weight.combat : d.weight.total;

            d.weight.counted = [
                getCounted(sil, countedWeigth, 0 + wbonus, -40),
                getCounted(sil, countedWeigth, 6 + wbonus, wbonus),
                getCounted(sil, countedWeigth, 12 + wbonus, 6 + wbonus),
                getCounted(sil, countedWeigth, 17 + wbonus, 12 + wbonus),
                getCounted(sil, countedWeigth, 21 + wbonus, 17 + wbonus),
            ];

            d.weight.limit = [
                Util.tableWeight(sil + 0 + wbonus),
                Util.tableWeight(sil + 6 + wbonus),
                Util.tableWeight(sil + 12 + wbonus),
                Util.tableWeight(sil + 17 + wbonus),
                Util.tableWeight(sil + 21 + wbonus)
            ];
            a.body.bonus -= Math.max(0, ((isCombat ? neededCombat : neededTotal) - sil) / 2);

            var i = 0;
            for (; i < 5; i++) {
                let w = d.weight.counted[i];
                if (w > 0 && w < 100) {
                    break;
                }
                if (w == 0) {
                    if (i > 0) i--;
                    break;
                }
            }
            if (i == 5 && (d.weight.limit[4] - countedWeigth) == 0) {
                i--;
            }
            if (i == 5) {
                ui.notifications.error(game.i18n.format("DRDP2.CharacterHasFullWeight", { character: this.actor.name }));
            }
            d.weight.weightInfo = d.config.weightInfo[i];
        })
    }

    async _prepareBackground(attributes, data) {

        const background = this.actor.itemTypes["background"].find(c => true);

        if (!background) {
            let name = game.i18n.localize(`ITEM.TypeBackground`);
            await this.actor.createOwnedItem({
                type: "background",
                name: name
            });
        }

        Util.add(background.data.data.attributes, attributes);
        let skillsPerLevel = game.settings.get("drdp2", "skillsPerLevel");
        attributes.skills.points =
            (data.data.details.level - 1) * skillsPerLevel
            + data.config.backgroundSkillCount[
            Number.parseInt(background.data.data.points.skills)
            ];
    }

    /* -------------------------------------------- */

    /**
     * Handle editing an existing Owned Item for the Actor
     * @param {Event} event   The originating click event
     * @private
     */
    async _onItemLink(event) {
        event.preventDefault();
        const li = event.currentTarget.closest(".item");
        const pack = li.dataset.pack;
        const origin = li.dataset.origin;

        const item = await Util.getItem(origin, pack);
        if (item == null) {
            throw new Error(`Origin Item '${pack}.${origin}' does not exist`)
        } else {
            item.sheet.render(true);
        }
    }

    /* -------------------------------------------- */

    /**
     * Handle deleting an existing Owned Item for the Actor
     * @param {Event} event   The originating click event
     * @private
     */
    async _onItemDelete(event) {
        event.preventDefault();
        const li = event.currentTarget.closest(".item");
        this.actor.deleteOwnedItem(li.dataset.itemId);
    }

    async _onItemEdit(event) {
        event.preventDefault();

        let input = event.currentTarget;
        const value = input.value;
        let dataset = input.dataset;
        let item = this.actor.items.get(dataset.item);
        let updates = {};

        if (["+", "-"].includes(value[0])) {
            let delta = parseFloat(value);
            input.value = getProperty(item.data, input.name) + delta;
        } else if (value[0] === "=") {
            input.value = value.slice(1);
        }

        updates[input.name] = Number.parseFloat(input.value);
        item.update(updates);
    }

    /**
     * Handle adding level to Skill-Link for the Actor
     * @param {Event} event   The originating click event
     * @private
     */
    async _onSkillLearn(event) {
        event.preventDefault();
        const li = event.currentTarget.closest(".item");

        const skill = this.actor.getOwnedItem(li.dataset.itemId);
        const level = skill.data.data.level ?? 1;

        skill.update({ "data.level": level + 1 });
    }

    /**
     * Handle removing level of Skill-Link or complete remove for the Actor
     * @param {Event} event   The originating click event
     * @private
     */
    async _onSkillUnlearn(event) {
        event.preventDefault();
        const li = event.currentTarget.closest(".item");

        const skill = this.actor.getOwnedItem(li.dataset.itemId);
        const level = skill.data.data.level ?? 1;

        if (level <= 0) {
            this.actor.deleteOwnedItem(li.dataset.itemId);
        } else {
            skill.update({ "data.level": level - 1 });
        }
    }

    async _onLevelAdd(event) {
        event.preventDefault();

        const level = this.actor.data.data.details.level;
        const max = Util.tableLives(level + 15);
        const xp = this.actor.data.data.details.xp.value - max;

        this.actor.update({
            "data.details.level": level + 1,
            "data.details.xp.value": xp
        });
    }

    async _onLevelRem(event) {
        event.preventDefault();

        const level = this.actor.data.data.details.level - 1;
        const max = Util.tableLives(level + 15);
        const xp = this.actor.data.data.details.xp.value + max;

        this.actor.update({
            "data.details.level": level,
            "data.details.xp.value": xp
        });
    }
}

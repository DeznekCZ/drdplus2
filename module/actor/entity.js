import { DRDP2 } from '../config.js';

/**
 * Extend the base Actor class to implement additional system-specific logic.
 */
export default class DRDP2Actor extends Actor {

    /* -------------------------------------------- */

    /** @override */
    prepareBaseData() {
        super.prepareBaseData();
    }

    /* -------------------------------------------- */

    /** @override */
    prepareDerivedData() {
        super.prepareDerivedData();
    }

    /* -------------------------------------------- */
    /*  Socket Listeners and Handlers
    /* -------------------------------------------- */

    /** @override */
    static async create(data, options = {}) {
        data.token = data.token || {};
        if (data.type === "character") {
            mergeObject(data.token, {
                vision: true,
                dimSight: 60,
                brightSight: 30,
                actorLink: true,
                disposition: 1
            }, { overwrite: false });
        }
        return super.create(data, options);
    }
}

import Util from "./copyutil.js"

/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function () {

    // Define template paths to load
    const templatePaths = [

        //// Actor Sheet Partials
        //"systems/dnd5e/templates/actors/parts/actor-traits.html",
        "systems/drdp2/templates/actors/parts/actor-inventory.html",
        //"systems/dnd5e/templates/actors/parts/actor-features.html",
        "systems/drdp2/templates/actors/parts/actor-skills.html",
        //"systems/dnd5e/templates/actors/parts/actor-spellbook.html",
        //"systems/dnd5e/templates/actors/parts/actor-effects.html",
        "systems/drdp2/templates/parts/attributes.html",

        //// Item Sheet Partials
        //"systems/dnd5e/templates/items/parts/item-action.html",
        //"systems/dnd5e/templates/items/parts/item-activation.html",
        "systems/drdp2/templates/items/parts/item-description.html",
        "systems/drdp2/templates/items/parts/skill-requirement.html",
        //"systems/dnd5e/templates/items/parts/item-mountable.html",
        "systems/drdp2/templates/items/component/container-filter.html",
    ];

    for (let i = 0; i < CONFIG.DRDP2.ComponentTypeTemplates.length; i++) {
        templatePaths.push(
            "systems/drdp2/templates/items/component/"
            + CONFIG.DRDP2.ComponentTypeTemplates[i] + ".html");
    }

    // Help functions
    Handlebars.registerHelper("log", function (data) {
        console.log(data)
        return ""
    });

    Handlebars.registerHelper("empty", function (data) {
        return !data;
    });

    Handlebars.registerHelper("not", function (data) {
        return !data;
    });

    Handlebars.registerHelper("is_true", function (data) {
        return data === true;
    });

    Handlebars.registerHelper("at", function (data, index) {
        return data[index];
    });

    Handlebars.registerHelper("setting", function (key) {
        let [group, setting] = key.split(".");
        return game.settings.get(group, setting);
    });

    Handlebars.registerHelper("romans", Util.romans);
    Handlebars.registerHelper("arabic", Util.arabic);

    Handlebars.registerHelper("attr_base", function (attributes, group, id) {
        //console.log(`${group}.${id}.base`);

        if (group.includes(".")) {
            let [key1, key2] = group.split(".");

            if (key1 in attributes) {
                let grp1 = attributes[key1];
                if (key2 in grp1) {
                    let grp2 = grp1[key2];
                    if (id in grp2) return grp2[id].base ?? 0;
                }
            }
        }
        else if (group in attributes) {
            let grp = attributes[group];
            if (id in grp) return grp[id].base ?? 0;
        }
        return 0;
    });

    Handlebars.registerHelper("attr_bonus", function (attributes, group, id) {
        //console.log(`${group}.${id}.bonus`);

        if (group.includes(".")) {
            let [key1, key2] = group.split(".");

            if (key1 in attributes) {
                let grp1 = attributes[key1];
                if (key2 in grp1) {
                    let grp2 = grp1[key2];
                    if (id in grp2) return grp2[id].bonus ?? 0;
                }
            }
        }
        else if (group in attributes) {
            let grp = attributes[group];
            if (id in grp) return grp[id].bonus ?? 0;
        }
        return 0;
    });

    Handlebars.registerHelper("attr_value", function (attributes, group, id) {
        //console.log(`${group}.${id}.value`);

        if (group.includes(".")) {
            let [key1, key2] = group.split(".");

            if (key1 in attributes) {
                let grp1 = attributes[key1];
                if (key2 in grp1) {
                    let grp2 = grp1[key2];
                    if (id in grp2) return grp2[id].value ?? 0;
                }
            }
        }
        else if (group in attributes) {
            let grp = attributes[group];
            if (id in grp) return grp[id].value ?? 0;
        }
        return 0;
    });

    Handlebars.registerHelper("attr", function (group, id) {
        return game.i18n.localize(`DRDP2.Attribute.${group}.${id}`)
    });

    Handlebars.registerHelper("concat", function (...text) {
        let array = Array.from(text);
        array.pop();
        return array.join("");
    });

    Handlebars.registerHelper("count", function (map) {
        return Object.keys(map ?? {}).length;
    });

    Handlebars.registerHelper("default_value", function (value, alternative) {
        return value ?? alternative;
    });

    Handlebars.registerHelper("times", function (context, n, content) {
        let result = "";

        content.data.context = context;

        for (var i = 0; i < n; i++) {
            content.data.index = i;
            result += content.fn(i);
        }

        return result;
    });

    Handlebars.registerHelper("times1", function (context, i0, n, content) {
        let result = "";

        content.data.context = context;
        content.data.index0 = i0;

        for (var i = 0; i < n; i++) {
            content.data.index = n * i0 + i;
            content.data.index1 = i;
            result += content.fn(i);
        }

        return result;
    });

    Handlebars.registerHelper("math", function (lvalue, operator, rvalue, options) {
        let lv = isNaN(lvalue) ? lvalue : parseFloat(lvalue);
        let rv = isNaN(rvalue) ? rvalue : parseFloat(rvalue);

        return {
            "+": lv + rv,
            "-": lv - rv,
            "*": lv * rv,
            "/": lv / rv,
            "%": lv % rv
        }[operator];
    });

    Handlebars.registerHelper("math_min", (a, b) => Math.min(a, b));
    Handlebars.registerHelper("math_max", (a, b) => Math.max(a, b));

    Handlebars.registerHelper("get_race", function (actorData) {
        const actor = game.actors.get(actorData._id);
        const race = actor.itemTypes["race-link"].find(c => true);

        if (!!race) {
            return new Handlebars.SafeString(`<a class="race-info" data-id="${race._id}">${race.name}</a>`);
        }

        return game.i18n.localize(`ITEM.TypeRace`)
    });

    Handlebars.registerHelper("get_background", function (actorData) {
        const actor = game.actors.get(actorData._id);
        const background = actor.itemTypes["background"].find(c => true);

        return new Handlebars.SafeString(`<a class="background-info" data-id="${background._id}">${background.name}</a>`);
    });

    Handlebars.registerHelper("checked", function (checked) {
        return new Handlebars.SafeString(checked == true ? "checked" : "");
    });

    Handlebars.registerHelper("object_total", function (arg) {
        let value = 0;
        let obj = arg ?? {};

        for (var key in obj) {
            let kv = obj[key];
            if (kv instanceof Number) {
                value += kv;
            }
        }

        return value;
    });

    Handlebars.registerHelper("object_total_recurse", function (outer) {
        function inner(arg) {
            let obj = arg;

            if (!isNaN(obj)) {
                return obj;
            }
            else if (!!obj) {
                let value = 0;

                for (var key in obj) {
                    let kv = obj[key];

                    try {
                        value += inner(kv);
                    } catch (e) {
                        // skip non eachable
                    }
                }

                return value;
            }
            else {
                return 0;
            }
        }

        return inner(outer);
    });

    Handlebars.registerHelper("component_template", function (type) {
        return "systems/drdp2/templates/items/component/" + type + ".html";
    });

    Handlebars.registerHelper('switch', function (value, inner) {
        this.switch_value = value;
        this.switch_done = false;
        return inner.fn(this);
    });

    Handlebars.registerHelper('case', function (value, inner) {
        if (!this.switch_done && value == this.switch_value) {
            this.switch_done = true;
            return inner.fn(this);
        }
    });

    Handlebars.registerHelper('common', function (inner) {
        if (this.switch_done) {
            return inner.fn(this);
        }
    });

    Handlebars.registerHelper('default', function (inner) {
        if (!this.switch_done) {
            return inner.fn(this);
        }
    });

    // Handle tables
    Handlebars.registerHelper('table', function (table, arg1, arg2, arg3) {
        return Util[table](arg1, arg2, arg3).toPrecision(2);
    });

    Handlebars.registerHelper('let', function (name, value) {
        this[name] = value;
    });

    Handlebars.registerHelper('global', function (name) {
        return window[name];
    });

    Handlebars.registerHelper("call", function (...data) {
        let array = Array.from(data);
        array.pop();

        let object = array.shift();
        let func = array.shift();
        let args = array;
        return object[func].apply(object, args);
    });

    Handlebars.registerHelper('get_value', function (name, data) {
        return Util.getValue(name, data);
    });

    Handlebars.registerHelper('float', function (value, precision) {
        if ((typeof value) == 'string')
            value = Number.parseFloat(value);
        let nv = value.toFixed(precision);
        while (nv.endsWith('0') && nv.includes('.') && precision > 0) {
            precision--;
            nv = value.toFixed(precision);
        }
        return nv;
    });

    Handlebars.registerHelper('getComponent', Util.getComponent);

    //Handlebars.registerHelper('eachComponent', function (type, components, inner) {
    //    let result = "";

    //    content.data.context = context;

    //    for (var i = 0; i < n; i++) {
    //        content.data.index = i;
    //    }

    //    return result;

    //    Util.eachComponent(type, components, component => {
    //        .path = 
    //        result += inner.fn(i);
    //    });
    //});

    Handlebars.registerHelper('if_then', function (condition, value) {
        return condition ? value : null;
    });

    // Load the template parts
    return loadTemplates(templatePaths);
};

// import {ClassFeatures} from "./classFeatures.js"

// Namespace Configuration Values
export const DRDP2 = {};

// ASCII Artwork
DRDP2.ASCII = `______________________________
______     ______       ____ 
|  _  \\    |  _  \\     /__  \\
| | | |_  _| | | |  _     / /
| | | | |/ | | | |_| |_  / /
| |_| |  _/| |_| |_   _|/ /_
|_____/_|  |_____/ |_| |____|
______________________________`;

DRDP2.defaultAmmoTypes = [
    "none", // Is not an ammo
    "self"  // Use self as thrown
];

DRDP2.linkTemplate = {
    _id: null,
    pack: null
};

DRDP2.workQuality = {
    "poor": 3,
    "sufficient": 6,
    "normal": 9,
    "better": 12,
    "handiwork": 16,
    "masterwork": 19
};

DRDP2.handItemSlots = [
    "main-hand",
    "off-hand",
    "two-hand"
];

DRDP2.bodyItemSlots = [
    "chest",
    "back",
    "head",
    "legs",
    "feet",
    "waist",
    "neck",
    "finger"
];

DRDP2.extraItemSlots = [
    // Throwable or knives
    // placeable into belt
    "small-weapon",
    // swords, axes, hammers, small crossbows, small guns
    // placeable into belt
    "short-weapon",
    // bigger sized weapons, crossbows
    // placeable on back
    "big-weapon",
    // bows, pikes, staffs
    // placeable on back
    "long-weapon"
];

DRDP2.weightInfo = [
    "DRDP2.WeightNone",
    "DRDP2.WeightLess",
    "DRDP2.WeightMedium",
    "DRDP2.WeightHigh",
    "DRDP2.WeightExtreme",
    "DRDP2.WeightOverMaximum"
];

DRDP2.damageTypes = {
    "physical": "DRDP2.DamagePhysical",
    "elemental": "DRDP2.DamageElemental",
    "psychical": "DRDP2.DamagePsychical"
}

DRDP2.physicalDamage = {
    "slash": "DRDP2.DamagePhysicalSlash",
    "pierce": "DRDP2.DamagePhysicalPierce",
    "blunt": "DRDP2.DamagePhysicalBlunt",
}

DRDP2.elementalDamage = {
    "fire-more": "DRDP2.DamageElementalFireMore",
    "fire-less": "DRDP2.DamageElementalFireLess",
    "water-more": "DRDP2.DamageElementalWaterMore",
    "water-less": "DRDP2.DamageElementalWaterLess",
    "air-more": "DRDP2.DamageElementalAirMore",
    "air-less": "DRDP2.DamageElementalAirLess",
    "earth-more": "DRDP2.DamageElementalEarthMore",
    "earth-less": "DRDP2.DamageElementalEarthLess"
}

DRDP2.ComponentTypeTemplates = [
    "weapon", "armor", "ammo", "container"
];

DRDP2.ComponentTypeTemplate = {
    weapon: {
        name: "DRDP2.ComponentWeapon",
        type: "weapon",
        twohand: false,
        attacks: [],
        defenses: []
    },

    armor: {
        name: "DRDP2.ComponentArmor",
        type: "armor",
        defenses: [],
        skills: []
    },

    ammo: {
        name: "DRDP2.ComponentAmmo",
        type: "ammo",
        value: 0,
        force: {
            minimum: 0,
            maximum: 0,
        },
        distance: {
            minimum: 0,
            maximum: 0,
        },
        damages: []
    },

    container: {
        name: "DRDP2.ComponentContainer",
        type: "container",
        weightLimit: 0,
        countLimit: 0,
        filters: [],
        itemlists: [],
        display: {
            weight: false,
            count: false,
            value: false
        }
    }
};

DRDP2.SubComponentTypeTemplate = {
    weaponAttack: {
        melee: {
            name: "DRDP2.WeaponMelee",
            type: "melee",
            value: 0,
            force: 0,
            skills: [],
            distance: 0,
            damages: []
        },

        ranged: {
            name: "DRDP2.WeaponRanged",
            type: "ranged",
            value: 0,
            force: {
                minimum: 0,
                maximum: 0,
            },
            distance: {
                minimum: 6,
                maximum: 26,
            },
            damages: [],
            skills: [],
            ammo: []
        },

        throwable: {
            name: "DRDP2.WeaponThrow",
            type: "throwable",
            value: 0,
            force: 0,
            skills: [],
            distance: {
                minimum: 6,
                maximum: 26,
            },
            damages: []
        }
    },

    weaponDamage: {
        physical: {
            name: DRDP2.damageTypes.physical,
            type: "physical",
            value: 0,
            effects: []
        },

        elemental: {
            name: DRDP2.damageTypes.elemental,
            type: "elemental",
            value: 0,
            element: "earth-less",
            effects: []
        }
    },

    weaponDefense: {
        melee: {
            name: "DRDP2.DefenseMelee",
            type: "melee",
            value: 0,
            skills: []
        },

        // Includes throw attack
        ranged: {
            name: "DRDP2.DefenseRanged",
            type: "ranged",
            value: 0,
            skills: []
        }
    },

    armorDefense: {
        physical: {
            name: DRDP2.damageTypes.physical,
            type: "physical",
            value: 0,
            damage: "any"
        },

        elemental: {
            name: DRDP2.damageTypes.elemental,
            type: "elemental",
            value: 0,
            element: "any-more"
        }
    },

    containerFilter: {
        accept: {
            name: "DRDP2.ContainerFilterAccept",
            type: "accept",
            types: {}
        },

        deny: {
            name: "DRDP2.ContainerFilterDeny",
            type: "deny",
            types: {}
        },

        items: {
            name: "DRDP2.ContainerFilterItems",
            type: "items",
            items: [] // item links
        },

        and: {
            name: "DRDP2.ContainerFilterAnd",
            type: "and",
            filters: []
        },

        or: {
            name: "DRDP2.ContainerFilterOr",
            type: "or",
            filters: []
        }
    }
};

DRDP2.skillReqType = {
    "or": "DRDP2.SkillReq.condition.or",
    "and": "DRDP2.SkillReq.condition.and",
    "not": "DRDP2.SkillReq.condition.not",
    "attribute": "DRDP2.SkillReq.attribute.option",
    "skill": "DRDP2.SkillReq.skill.option"
};

DRDP2.skillGroup = {
    "none": "DRDP2.SkillGroup.none",
    "racial": "DRDP2.SkillGroup.racial",
    "combat": "DRDP2.SkillGroup.combat",
    "class": "DRDP2.SkillGroup.class"
};

DRDP2.backgroundTypeValues = {
    "background": 15,
    "combined": 10,
    "attributes": 5
};

DRDP2.backgroundAttributePoints = {
    "background": { single: 1, total: 3 },
    "combined": { single: 2, total: 6 },
    "attributes": { single: 3, total: 9 }
};

DRDP2.backgroundSkillCount = {
    0: 10,
    1: 11,
    2: 12,
    3: 14,
    4: 16,
    5: 18,
    6: 20,
    7: 22,
    8: 24
}

DRDP2.backgroundFamilyType = {
    0: "waif",
    1: "orphan",
    2: "single-parent-family",
    3: "doubtful-family",
    4: "decent-family",
    5: "good-family",
    6: "influential-family",
    7: "poor-nobleman",
    8: "nobleman"
}

DRDP2.attributeList = {
    "none": "DRDP2.Attribute.none",
    "data.attributes.body.sil": "DRDP2.Attribute.body.sil",
    "data.attributes.body.obr": "DRDP2.Attribute.body.obr",
    "data.attributes.body.zrc": "DRDP2.Attribute.body.zrc",
    "data.attributes.mind.vol": "DRDP2.Attribute.mind.vol",
    "data.attributes.mind.int": "DRDP2.Attribute.mind.int",
    "data.attributes.mind.chr": "DRDP2.Attribute.mind.chr",
    "data.attributes.reaction.odl": "DRDP2.Attribute.reaction.odl",
    "data.attributes.reaction.vdr": "DRDP2.Attribute.reaction.vdr",
    "data.attributes.reaction.rch": "DRDP2.Attribute.reaction.rch",
    "data.attributes.reaction.sms": "DRDP2.Attribute.reaction.sms",
    "data.attributes.reaction.mag": "DRDP2.Attribute.reaction.mag",
    "data.attributes.senses.sms-zrk": "DRDP2.Attribute.senses.sms-zrk",
    "data.attributes.senses.sms-slu": "DRDP2.Attribute.senses.sms-slu",
    "data.attributes.senses.sms-cch": "DRDP2.Attribute.senses.sms-cch",
    "data.attributes.senses.sms-hmt": "DRDP2.Attribute.senses.sms-hmt",
    "data.attributes.senses.sms-cht": "DRDP2.Attribute.senses.sms-cht",
    "data.attributes.senses.sms-vnm": "DRDP2.Attribute.senses.sms-vnm",
    "data.attributes.visage.krs": "DRDP2.Attribute.visage.krs",
    "data.attributes.visage.neb": "DRDP2.Attribute.visage.neb",
    "data.attributes.visage.dst": "DRDP2.Attribute.visage.dst",
    "data.attributes.sizes.vel": "DRDP2.Attribute.sizes.vel",
    "data.attributes.sizes.hmp": "DRDP2.Attribute.sizes.hmp",
    "data.attributes.combat.init.melee": "DRDP2.Attribute.combat.init.melee",
    "data.attributes.combat.init.ranged": "DRDP2.Attribute.combat.init.ranged",
    "data.attributes.combat.init.spell": "DRDP2.Attribute.combat.init.spell",
    "data.attributes.combat.attack.melee": "DRDP2.Attribute.combat.attack.melee",
    "data.attributes.combat.attack.ranged": "DRDP2.Attribute.combat.attack.ranged",
    "data.attributes.combat.defense.melee": "DRDP2.Attribute.combat.defense.melee",
    "data.attributes.combat.defense.ranged": "DRDP2.Attribute.combat.defense.ranged",
    "data.attributes.skills": "DRDP2.Attribute.skills",
    "data.attributes.hp.bonus-lines": "DRDP2.Attribute.hp.bonus-lines",
    "data.attributes.hp.pain": "DRDP2.Attribute.hp.pain",
    "data.attributes.hp.damage": "DRDP2.Attribute.hp.damage",
    "data.attributes.fatigue.bonus-lines": "DRDP2.Attribute.fatigue.bonus-lines",
    "data.attributes.fatigue.tired": "DRDP2.Attribute.fatigue.tired",
    "data.attributes.fatigue.level": "DRDP2.Attribute.fatigue.level",
};

DRDP2.mainAttributes = {
    "body": {
        "sil": {
            "value": 0
        },
        "obr": {
            "value": 0
        },
        "zrc": {
            "value": 0
        }
    },

    "mind": {
        "vol": {
            "value": 0
        },
        "int": {
            "value": 0
        },
        "chr": {
            "value": 0
        }
    },

    "reaction": {
        "odl": {
            "value": 0
        },
        "vdr": {
            "value": 0
        },
        "rch": {
            "value": 0
        },
        "sms": {
            "value": 0
        },
        "mag": {
            "value": 0
        }
    },

    "senses": {
        "sms-zrk": {
            "value": 0
        },
        "sms-slu": {
            "value": 0
        },
        "sms-cch": {
            "value": 0
        },
        "sms-hmt": {
            "value": 0
        },
        "sms-cht": {
            "value": 0
        },
        "sms-vnm": {
            "value": 0
        }
    },

    "visage": {
        "krs": {
            "value": 0
        },
        "neb": {
            "value": 0
        },
        "dst": {
            "value": 0
        }
    },

    "sizes": {
        "vel": {
            "value": 0
        },
        "hmp": {
            "value": 0
        }
    },

    "combat": {
        "init": {
            "melee": {
                "value": 0
            },
            "ranged": {
                "value": 0
            },
            "spell": {
                "value": 0
            }
        },

        "attack": {
            "melee": {
                "value": 0
            },
            "ranged": {
                "value": 0
            }
        },

        "defense": {
            "melee": {
                "value": 0
            },
            "ranged": {
                "value": 0
            }
        }
    }
};
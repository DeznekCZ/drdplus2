﻿import Util from "../copyutil.js";

/**
 * Override and extend the basic :class:`Item` implementation
 */
export default class DRDP2Item extends Item {

    getAtIndex(path, index) {
        let origin = duplicate(this.data);
        let array = Util.getValue(path, origin);

        return array[index];
    }

    async removeAtIndex(path, index) {
        let origin = duplicate(this.data);
        let array = Util.getValue(path, origin);

        array.splice(index, 1);

        return super.update(origin);
    }

    async replaceAtIndex(path, index, value) {
        let origin = duplicate(this.data);
        let array = Util.getValue(path, origin);

        array[index] = value;

        return super.update(origin);
    }

    async addAtIndex(path, data, index = -1) {
        let origin = duplicate(this.data);
        let array = Util.getValue(path, origin);

        if (index == -1) {
            array.push(data);
        } else {
            array.splice(index, 0, data);
        }

        return super.update(origin);
    }

    async update(data, options = {}) {
        if (options.optimized) {
            return super.update(data, options);
        } else {
            let expanded = expandObject(data);
            let array = Util.fixArrays(expanded);

            let origin = duplicate(this.data);
            Util.merge(origin, array);

            return super.update(origin, options);
        }
    }
}

// ƒ mergeObject(original, other={}, {     insertKeys=true,     insertValues=true,     overwrite=true,     recursive=true,     inplace=true,     enforceTypes=false   }={}, _d=0
//import TraitSelector from "../apps/trait-selector.js";
import Util from "../copyutil.js";

export default class DRDP2ItemSheet extends ItemSheet {
    constructor(...args) {
        super(...args);

        // Expand size of the class sheet
        if (this.object.data.type === "race") {
            this.options.width = 560;
            this.options.height = "auto";
            this.options.classes = ["drdp2", "sheet", "item", "race"];
        }
    }

    get template() {
        return `systems/drdp2/templates/items/${this.item.data.type}.html`;
    }

    async getData() {
        const data = super.getData();

        data.config = CONFIG.DRDP2;
        data.itemProperties = data.itemProperties ?? [];
        data.itemProperties.push(data.item.type);

        if (data.item.type == "background") {
            let points = data.data.points;
            points.value = points.limit
                - Number.parseInt(points.skills)
                - Number.parseInt(points.origin)
                - points.ownage;
        }

        if (data.item.type == "item") {
            let components = data.data.components;
            for (var p = 0; p < components.length; p++) {
                let comp = components[p];
                if (!data.itemProperties.includes(comp.type))
                    data.itemProperties.push(comp.type);
            }

            let weapon = Util.getComponent('weapon', components);
            if (weapon != null) {
                if (weapon.twohand) {
                    data.itemProperties.push('twohand');
                } else {
                    data.itemProperties.push('onehand');
                }
                for (var p = 0; p < weapon.attacks.length; p++) {
                    let comp = weapon.attacks[p];
                    if (!data.itemProperties.includes(comp.type))
                        data.itemProperties.push(comp.type);
                }

                let attack = Util.getComponent('ranged', weapon.attacks);
                if (attack != null) {
                    let ammoes = attack.ammo;
                    for (var i = 0; i < ammoes.length; i++) {
                        let ammo = ammoes[i];
                        if (ammo._id != null) {
                            let item = await Util.getItem(ammo._id, ammo.pack);
                            if (item != null) {
                                let ammoc = Util.getComponent('ammo', item.data.data.components);
                                ammo.data = duplicate(ammoc);
                            }
                        }
                    }
                }
            }
        }

        for (var p = 0; p < data.itemProperties.length; p++) {
            let comp = data.itemProperties[p];

            data.itemProperties[p] = game.i18n.localize(
                'ITEM.Type'
                + comp.substring(0, 1).toUpperCase()
                + comp.substring(1)
            );
        }

        return data;
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            width: 560,
            height: 400,
            classes: ["drdp2", "sheet", "item"],
            resizable: true,
            scrollY: [
                ".tab.details",
                ".tab.requirements",
                ".scrollY"
            ],
            scrollX: [
                ".tab.attributes"
            ],
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "description" }]
        });
    }

    /**
     * Activate event listeners using the prepared sheet HTML
     * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
     */
    activateListeners(html) {
        if (!this.options.editable) {
            super.activateListeners(html);
            return;
        }

        html.find(".filter")
            .on("drop", this._onFilterDrop.bind(this))
            .on("dragstart", this._onFilterDrag.bind(this));

        html.find(".item-drop")
            .on("drop", this._onDropItemCreate.bind(this));

        html.find(".item-drop-link")
            .on("dragstart", this._onDragItemStart.bind(this));

        html.find(".item-drop-link").click(this._onItemView.bind(this));

        if (this.item.type == "skill") {
            html.find(".new-requirement").click(this._addRequirement.bind(this))
            html.find(".delete-requirement").click(this._deleteRequirement.bind(this))
        }

        if (this.item.type == "background") {
            html.find(".background-type").click(this._onBackgroundTypeChange.bind(this));
            html.find(".background-ownage").change(this._onBackgroundOwnageChange.bind(this));
        }

        if (this.item.type == "item") {
            html.find(".add-component").click(this._onComponentAdd.bind(this));
            html.find(".remove-component").click(this._onComponentRemove.bind(this));
        }

        html.find('.attribute-score').change(this._onAttributeSet.bind(this));

        super.activateListeners(html);
    }

    async _onItemView(event) {
        event.preventDefault();

        let dataset = event.currentTarget.dataset;
        let id = dataset.id;
        let packid = dataset.pack;

        let item = game.items.get(id);

        if (packid.length > 0) {
            let pack = game.packs.get(packid);
            if (pack === null) {
                throw new Error(`Missing pack '${packid}'`);
            } else {
                item = await pack.getEntity(id);

                if (item === null) {
                    throw new Error(`Missing item '${packid}.${id}'`);
                } else {
                    item.sheet.render(true);
                }
            }
        } else if (item !== null) {
            item.sheet.render(true);
        } else {
            throw new Error(`Missing item '${id}'`);
        }
    }

    async _onAttributeSet(event) {
        event.preventDefault();

        let target = event.currentTarget;
        let name = target.name;
        let value = Math.max(0, Number.parseInt(target.value));

        if (!!target.value || value != 0) {
            let updates = {};
            updates[name] = value;
            this.item.update(updates);
        } else {
            let updates = {};
            updates[name] = 0;
            this.item.update(updates);
        }
    }

    async _onDropItemCreate(event) {
        event.preventDefault();
        let data = JSON.parse(event.originalEvent.dataTransfer.getData('text/plain'));

        if (data.type === "Item") {
            let pack = game.packs.get(data.pack);
            let item = game.items.get(data.id);

            if (item == null && pack !== null) {
                item = await pack.getEntity(data.id);
            }
            let dataset = event.currentTarget.dataset;
            let path = dataset.path;

            if (Util.same("skill", item.type, dataset.itemtype) && dataset.old == "true") {
                let updates = {}
                updates[`${path}.-=skill`] = null;
                updates[`${path}.skill._id`] = data.id;
                updates[`${path}.skill.name`] = item.name;
                updates[`${path}.skill.pack`] = data.pack ?? "";
                updates[`${path}.skill.level`] = 0;
                await this.item.update(updates,
                    { optimized: true });
            } else if (item.type == dataset.itemtype) {
                let properties = (dataset.properties?.split(" ") ?? []);
                properties.splice(0, 0, "id", "item.name", "pack");
                let updates = {};

                for (var i = 0; i < properties.length; i++) {
                    let property = properties[i];

                    if (property.startsWith("item.")) {
                        let itemProperty = property.substring(5);
                        updates[path + "." + itemProperty] = Util.getValue(itemProperty, item);
                    } else if (property == "id") {
                        updates[path + "._id"] = data.id;
                    } else {
                        updates[path + "." + property] = Util.getValue(property, data) ?? "";
                    }
                }

                this.item.update(updates);
            }
        }
    }

    async _onDragItemStart(event) {
        event.dataTransfer.setData('text/plain', JSON.stringify({
            type: 'Item',
            id: event.target.dataset.id,
            pack: event.target.dataset.pack
        }))
    }

    async _onFilterDrop(event) {
        event.preventDefault();
        let data = JSON.parse(event.originalEvent.dataTransfer.getData('text/plain'));

        if (data.type == 'Filter') {
            let dataset = event.target.dataset;
            let index = Number.parseInt(dataset.index);
            let path = dataset.path;

            let filterNew = this.item.getAtIndex(data.path, data.index);
            let filterOld = this.item.getAtIndex(path, index);
            await this.item.replaceAtIndex(data.path, data.index, filterOld);
            await this.item.replaceAtIndex(path, index, filterNew);
        }
    }

    async _onFilterDrag(event) {
        let dataset = event.target.dataset;
        event.originalEvent.dataTransfer.setData('text/plain', JSON.stringify({
            type: 'Filter',
            index: Number.parseInt(dataset.index),
            path: dataset.path
        }))
    }

    async _addRequirement(event) {
        event.preventDefault();

        let dataset = event.currentTarget.dataset;
        let path = dataset.reqContext;

        let dupl = duplicate(this.item.data);
        let reqs = Util.getData(dupl, path, {});

        for (var i = 0; true; i++) {
            if (!(`_${i}` in reqs)) {
                reqs[`_${i}`] = {
                    "type": "and",
                    "requirements": {}
                }
                break;
            }
        }

        return this.item.update({
            "data.requirements": dupl.data.requirements
        },
            { optimized: true });
    }

    async _deleteRequirement(event) {
        event.preventDefault();

        let dataset = event.currentTarget.dataset;
        let path = dataset.reqContext;
        let id = dataset.reqId;

        let updates = {};
        updates[`${path}.-=${id}`] = null;
        return this.item.update(updates,
            { optimized: true });
    }

    async _onBackgroundTypeChange(event) {
        event.preventDefault();

        let typeId = event.currentTarget.id;

        let updates = {};
        updates["data.type"] = typeId;
        updates["data.points.limit"] = CONFIG.DRDP2.backgroundTypeValues[typeId];
        updates["data.points.attributes"] = CONFIG.DRDP2.backgroundAttributePoints[typeId];
        return this.item.update(updates,
            { optimized: true });
    }

    async _onBackgroundOwnageChange(event) {

    }

    async _onComponentAdd(event) {
        event.preventDefault();

        let dataset = event.currentTarget.dataset
        let componentObject = Util.getValue(dataset.componentType, CONFIG.DRDP2);

        this.item.addAtIndex(
            dataset.path,
            componentObject
        );
    }

    async _onComponentRemove(event) {
        event.preventDefault();

        let dataset = event.currentTarget.dataset;

        this.item.removeAtIndex(
            dataset.path,
            Number.parseInt(dataset.index)
        );
    }
}
